/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;


/**
 *
 * @author manhg
 */
public class Users {
    private String manv;
    private String fullname;
    private String birthday;
    private String password;
    private int id_phong;
    private String tenPhong;
    private String tienDoHoanThanh;
    private int document;
    private int document_ori;
    private int idPhong;

    public Users(String manv, String fullname, String birthday, String password, int id_phong, String tenPhong, int document, int document_ori) {
        this.manv = manv;
        this.fullname = fullname;
        this.birthday = birthday;
        this.password = password;
        this.id_phong = id_phong;
        this.tenPhong = tenPhong;
        this.document = document;
        this.document_ori = document_ori;
    }

    public Users(String manv, String fullname, String birthday, String password, int document, int document_ori) {
        this.manv = manv;
        this.fullname = fullname;
        this.birthday = birthday;
        this.password = password;
        this.document = document;
        this.document_ori = document_ori;
        this.idPhong = idPhong;
    }

    public int getId_phong() {
        return id_phong;
    }

    public void setId_phong(int id_phong) {
        this.id_phong = id_phong;
    }
    public Users(int idPhong) {
        this.idPhong = idPhong;
    }

    public int getIdPhong() {
        return idPhong;
    }

    public void setIdPhong(int idPhong) {
        this.idPhong = idPhong;
    }
    public String getTenPhong() {
        return tenPhong;
    }

    public void setTenPhong(String tenPhong) {
        this.tenPhong = tenPhong;
    }

    public String getTienDoHoanThanh() {
        return getDocument_ori()-getDocument() == 0 ? "Hoàn thành":"Chưa hoàn thành";
    }

    public void setTienDoHoanThanh(String tienDoHoanThanh) {
        this.tienDoHoanThanh = tienDoHoanThanh;
    }
    
    public Users(String manv, int document) {
        this.manv = manv;
        this.document = document;
    }

    public Users(String manv, String fullname, int document) {
        this.manv = manv;
        this.fullname = fullname;
        this.document = document;
    }

    public Users(String manv, String password) {
        this.manv = manv;
        this.password = password;
    }

    public String getManv() {
        return manv;
    }

    public void setManv(String manv) {
        this.manv = manv;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getDocument() {
        return document;
    }

    public void setDocument(int document) {
        this.document = document;
    }

    public int getDocument_ori() {
        return document_ori;
    }

    public void setDocument_ori(int document_ori) {
        this.document_ori = document_ori;
    }
    
}
