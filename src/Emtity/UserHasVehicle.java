/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

import java.util.Date;

/**
 *
 * @author manhg
 */
public class UserHasVehicle {
    private int vehicleId;
    private int distance;
    private int time;
    private Date startDate;

    public UserHasVehicle(int vehicleId, int distance, int time, Date startDate) {
        this.vehicleId = vehicleId;
        this.distance = distance;
        this.time = time;
        this.startDate = startDate;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    
}
