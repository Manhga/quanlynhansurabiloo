/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import quanlyhosorabiloo.GetListDocument.TableListTaiLieu;

/**
 *
 * @author hoang
 */
public class TableTaiLieu  extends AbstractTableModel{
    private final String name[] = {"Mã nhân viên","Tên nhân viên","Ngày Sinh","Phòng","Tiến độ hoàn thành"};
    private final Class classes[] = {String.class,String.class,String.class,String.class,String.class};
    ArrayList<TableListTaiLieu> list = new ArrayList<>();
    public  TableTaiLieu (ArrayList<TableListTaiLieu> lst)
    {
        this.list = lst;
    }
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return name.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return list.get(rowIndex).getManv();
            case 1: return list.get(rowIndex).getTennv();
            case 2: return list.get(rowIndex).getNgaysinh();
            case 3: return list.get(rowIndex).getPhong();
            case 4: return list.get(rowIndex).getTiendo();
        }
        return null;
    }

    

    @Override
    public String getColumnName(int column) {
        return name[column]; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
