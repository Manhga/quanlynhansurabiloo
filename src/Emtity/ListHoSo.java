/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

import java.awt.Button;

/**
 *
 * @author manhg
 */
public class ListHoSo {
    private int id;
    private int id_document;
    private String LoaiHoso;
    private String Status;
    private int status_id;

    public ListHoSo(int id, int id_document, String LoaiHoso, String Status, int status_id) {
        this.id = id;
        this.id_document = id_document;
        this.LoaiHoso = LoaiHoso;
        this.Status = Status;
        this.status_id = status_id;
    }

    public ListHoSo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_document() {
        return id_document;
    }

    public void setId_document(int id_document) {
        this.id_document = id_document;
    }

    public String getLoaiHoso() {
        return LoaiHoso;
    }

    public void setLoaiHoso(String LoaiHoso) {
        this.LoaiHoso = LoaiHoso;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }
    

}
 