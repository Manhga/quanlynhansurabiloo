/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

/**
 *
 * @author manhg
 */
public class PhanQuyen {
    private String manv;
    private int id_role;
    private String fullname;

    public PhanQuyen(String manv, int id_role, String fullname) {
        this.manv = manv;
        this.id_role = id_role;
        this.fullname = fullname;
    }

    
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getManv() {
        return manv;
    }

    public void setManv(String manv) {
        this.manv = manv;
    }

    public int getId_role() {
        return id_role;
    }

    public void setId_role(int id_role) {
        this.id_role = id_role;
    }
    
}
