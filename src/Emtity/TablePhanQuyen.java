/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author manhg
 */
public class TablePhanQuyen extends AbstractTableModel{
    private final String name[] = {"Mã NV","Họ tên","Quyền"};
    private final Class classes[] = {String.class, String.class, Integer.class};
    ArrayList<PhanQuyen> dsPQ = new ArrayList<PhanQuyen>();

    public TablePhanQuyen(ArrayList<PhanQuyen> dsPQ) {
        this.dsPQ = dsPQ;
    }


    @Override
    public int getRowCount() {
         return dsPQ.size();
    }

    @Override
    public int getColumnCount() {
        return name.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return dsPQ.get(rowIndex).getManv();
            case 1: return dsPQ.get(rowIndex).getFullname();
            case 2: return dsPQ.get(rowIndex).getId_role();
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return classes[columnIndex]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnName(int column) {
        return name[column];
    }
}
