/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

/**
 *
 * @author admin
 */
public class ListEmployee {
    private String manv;
    private String fullName;
    private String status;

    public ListEmployee(String manv, String fullName, String status) {
        this.manv = manv;
        this.fullName = fullName;
        this.status = status;
    }

    public ListEmployee() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getManv() {
        return manv;
    }

    public void setManv(String manv) {
        this.manv = manv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
