/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

/**
 *
 * @author manhg
 */
public class EmployeeInfor {
    private String comment;
    private String sodt;
    private int LoaiNha;
    private String ChuNha;
    private String OCung;
    private String diaChi;
    private String hoTen;
    private int quanHe;
    private int lienHe;
    private int TrinhDo;
    private String truongHoc;

    public EmployeeInfor() {
    }

    public EmployeeInfor(String comment, String sodt, int LoaiNha, String ChuNha, String OCung, String diaChi, String hoTen, int quanHe, int lienHe, int TrinhDo, String truongHoc) {
        this.comment = comment;
        this.sodt = sodt;
        this.LoaiNha = LoaiNha;
        this.ChuNha = ChuNha;
        this.OCung = OCung;
        this.diaChi = diaChi;
        this.hoTen = hoTen;
        this.quanHe = quanHe;
        this.lienHe = lienHe;
        this.TrinhDo = TrinhDo;
        this.truongHoc = truongHoc;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSodt() {
        return sodt;
    }

    public void setSodt(String sodt) {
        this.sodt = sodt;
    }

    public int getLoaiNha() {
        return LoaiNha;
    }

    public void setLoaiNha(int LoaiNha) {
        this.LoaiNha = LoaiNha;
    }

    public String getChuNha() {
        return ChuNha;
    }

    public void setChuNha(String ChuNha) {
        this.ChuNha = ChuNha;
    }

    public String getOCung() {
        return OCung;
    }

    public void setOCung(String OCung) {
        this.OCung = OCung;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getQuanHe() {
        return quanHe;
    }

    public void setQuanHe(int quanHe) {
        this.quanHe = quanHe;
    }

    public int getLienHe() {
        return lienHe;
    }

    public void setLienHe(int lienHe) {
        this.lienHe = lienHe;
    }

    public int getTrinhDo() {
        return TrinhDo;
    }

    public void setTrinhDo(int TrinhDo) {
        this.TrinhDo = TrinhDo;
    }

    public String getTruongHoc() {
        return truongHoc;
    }

    public void setTruongHoc(String truongHoc) {
        this.truongHoc = truongHoc;
    }
    
}
