/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

/**
 *
 * @author manhg
 */
public class UserDetail {
    private int levelId;
    private String education;
    private String address;
    private int idTypeResidence;
    private String nameOfHost;
    private String theSamePerson;
    private int relationshipId;
    private String userComment;
    private String managerComment;
    private int phone;
    private String name_family;
    private String family_address;

    public UserDetail(int levelId, String education, String address, int idTypeResidence, String nameOfHost, String theSamePerson, int relationshipId, String userComment, String managerComment, int phone, String name_family, String family_address) {
        this.levelId = levelId;
        this.education = education;
        this.address = address;
        this.idTypeResidence = idTypeResidence;
        this.nameOfHost = nameOfHost;
        this.theSamePerson = theSamePerson;
        this.relationshipId = relationshipId;
        this.userComment = userComment;
        this.managerComment = managerComment;
        this.phone = phone;
        this.name_family = name_family;
        this.family_address = family_address;
    }

    public UserDetail() {
    }

    public String getName_family() {
        return name_family;
    }

    public void setName_family(String name_family) {
        this.name_family = name_family;
    }

    public String getFamily_address() {
        return family_address;
    }

    public void setFamily_address(String family_address) {
        this.family_address = family_address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIdTypeResidence() {
        return idTypeResidence;
    }

    public void setIdTypeResidence(int idTypeResidence) {
        this.idTypeResidence = idTypeResidence;
    }

    public String getNameOfHost() {
        return nameOfHost;
    }

    public void setNameOfHost(String nameOfHost) {
        this.nameOfHost = nameOfHost;
    }

    public String getTheSamePerson() {
        return theSamePerson;
    }

    public void setTheSamePerson(String theSamePerson) {
        this.theSamePerson = theSamePerson;
    }

    public int getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(int relationshipId) {
        this.relationshipId = relationshipId;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public String getManagerComment() {
        return managerComment;
    }

    public void setManagerComment(String managerComment) {
        this.managerComment = managerComment;
    }
    
}
