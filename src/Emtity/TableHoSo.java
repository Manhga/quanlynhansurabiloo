/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author manhg
 */
public class TableHoSo extends AbstractTableModel{
    private final String name[] = {"Mã","Mã tài liệu","Tên Tài Liệu","Trạng thái"};
    private final Class classes[] = {int.class, int.class, String.class, String.class};
    ArrayList<ListHoSo> dsHs = new ArrayList<ListHoSo>();

    public TableHoSo(ArrayList<ListHoSo> dsHs) {
        this.dsHs = dsHs;
    }

    @Override
    public int getRowCount() {
        return dsHs.size(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return name.length; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return dsHs.get(rowIndex).getId();
            case 1: return dsHs.get(rowIndex).getId_document();
            case 2: return dsHs.get(rowIndex).getLoaiHoso();
            case 3: return dsHs.get(rowIndex).getStatus();
        }
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return classes[columnIndex]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnName(int column) {
        return name[column];
    }
    
}
