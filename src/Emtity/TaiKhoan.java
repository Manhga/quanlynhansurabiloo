/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

/**
 *
 * @author manhg
 */
public class TaiKhoan {
    private String Ten, MK, ThongBao;

    public TaiKhoan(String Ten, String MK, String ThongBao) {
        this.Ten = Ten;
        this.MK = MK;
        this.ThongBao = ThongBao;
    }

    public TaiKhoan() {
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public String getMK() {
        return MK;
    }

    public void setMK(String MK) {
        this.MK = MK;
    }

    public String getThongBao() {
        return ThongBao;
    }

    public void setThongBao(String ThongBao) {
        this.ThongBao = ThongBao;
    }
    
}
