/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Emtity;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author admin
 */
public class TableNhanVien extends AbstractTableModel{

    private final String name[]={"Mã","FullName","Tiến Độ"};
    private final Class classes[] = {String.class, String.class, String.class};
    ArrayList<ListEmployee> dsNV= new ArrayList<>();

    public TableNhanVien(ArrayList<ListEmployee> dsNV) {
        this.dsNV=dsNV;
    }    
    
    @Override
    public int getRowCount() {
        return dsNV.size();
         //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return name.length;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return dsNV.get(rowIndex).getManv();
            case 1: return dsNV.get(rowIndex).getFullName();
            case 2: return dsNV.get(rowIndex).getStatus();
            
        }
        return null;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return classes[columnIndex]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnName(int column) {
        return name[column];
    }
    
}
