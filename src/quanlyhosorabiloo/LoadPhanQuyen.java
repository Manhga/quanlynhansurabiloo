/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosorabiloo;

import Emtity.PhanQuyen;
import Model.ConnectedDatabase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author manhg
 */
public class LoadPhanQuyen{
    ConnectedDatabase cnn = new ConnectedDatabase();
    ArrayList<PhanQuyen> dsPQ = new ArrayList<>();

    public ArrayList<PhanQuyen> getDsPQ() {
        return dsPQ;
    }

    public void setDsPQ(ArrayList<PhanQuyen> dsPQ) {
        this.dsPQ = dsPQ;
    }
    public boolean checkLoadAcc(String manv){
        try{
            cnn.ConnectedDB();
            try { 
            String ma = null;
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users where manv = ?");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                ma = result.getString("manv");
            }
            if(ma != null){
                return true;
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        }catch(SQLException ex){
            Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public ArrayList<PhanQuyen> LoadTable(){
        ArrayList<PhanQuyen> dsPQ = new ArrayList<>();
        try {
            cnn.ConnectedDB();
            try {        
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users, user_has_roles where Users.manv = user_has_roles.manv and user_has_roles.id_role <> ?");
            stmt.setInt(1, 1);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                String manv = result.getString("manv");
                String fullname = result.getString("fullname");
                int id_role = result.getInt("id_role");
                System.out.println(manv);
                System.out.println(fullname);
                dsPQ.add(new PhanQuyen(manv, id_role, fullname));
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
        return dsPQ;
    }
    
    public boolean UpdateRole(String manv, int id_role){
        int row = 0;
        try {
            cnn.ConnectedDB();
            try {
            PreparedStatement stmt = cnn.connection.prepareStatement("Update user_has_roles set id_role =? where manv= ?");
            stmt.setInt(1, id_role);
            stmt.setString(2, manv);
            stmt.executeUpdate();
            PreparedStatement st = cnn.connection.prepareStatement("Update Users set total_document_up =? where manv= ?");
            st.setInt(1, 2);
            st.setString(2, manv);
            row = st.executeUpdate();
            if(row >0){
                return true;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,ex);
        } finally {
            CloseConnect();
        }
        } catch (SQLException ex) {
            Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
}
    
    public boolean InsertEmployee(String fullname, String ngaySinh, String manv, String password, int phong, int id_role){
        try {
            cnn.ConnectedDB();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.sql.Date birthDay;
            try{
                birthDay = new java.sql.Date(formatter.parse(ngaySinh).getTime());
                try {
                int total_doc = 4;
                int total_doc_put = 0;
                long millis=System.currentTimeMillis();  
                java.sql.Date date=new java.sql.Date(millis);
                PreparedStatement stmt = cnn.connection.prepareStatement("Insert into Users values(?,?,?,?,?,?,?,?)");
                stmt.setString(1, fullname);
                stmt.setDate(2, birthDay);
                stmt.setString(3, manv);
                stmt.setString(4, password);
                stmt.setInt(5, total_doc_put);
                stmt.setInt(6, total_doc);
                stmt.setInt(7, phong);
                stmt.setDate(8, date);
                stmt.executeUpdate();
                PreparedStatement st = cnn.connection.prepareStatement("Insert into user_has_roles values(?,?)");
                st.setInt(1, id_role);
                st.setString(2, manv);
                st.executeUpdate();
                for(int i = 0; i<4;i++){
                    PreparedStatement stt = cnn.connection.prepareStatement("Insert into user_has_documents values(?,?,?)");
                    stt.setString(1, manv);
                    stt.setInt(2, i+1);
                    stt.setInt(3, 1);
                    stt.executeUpdate();
                }
                return true;
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null,ex);
            } finally {
                CloseConnect();
            }
            }catch(ParseException ex){
                Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            }
            } catch (SQLException ex) {
                Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
            }
        return false;
    }

    private void CloseConnect() {
         try {
                cnn.connection.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
    }
    
}
