/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosorabiloo;

import Emtity.UserDetail;
import Model.ConnectedDatabase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author hoang
 */
public class NS_GetTaiLieuCaNhan {
    ConnectedDatabase cnn = new ConnectedDatabase();
    public UserDetail getUserDetail(String manv) throws SQLException {
        cnn.ConnectedDB();
        PreparedStatement stmt = cnn.connection.prepareStatement("SELECT *  FROM detail_users  where manv = ?");
        stmt.setString(1, manv);
        ResultSet result = stmt.executeQuery();
        UserDetail ust = null;
        while(result.next())
        {
            ust = new UserDetail();
            ust.setLevelId(result.getInt("Level_id"));
            ust.setEducation(result.getString("education"));
            ust.setAddress(result.getString("address"));
            ust.setIdTypeResidence(result.getInt("id_type_of_residence"));
            ust.setNameOfHost(result.getString("name_of_host"));
            ust.setTheSamePerson(result.getString("the_same_person"));
            ust.setName_family(result.getString("family_user_name"));
            ust.setFamily_address(result.getString("family_address"));
            ust.setRelationshipId(result.getInt("relationship_id"));
            ust.setUserComment(result.getString("user_comment"));
            ust.setManagerComment(result.getString("manager_comment"));
            ust.setPhone(result.getInt("phone"));
        }
        return ust;
    }
}
