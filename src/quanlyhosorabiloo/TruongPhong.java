/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosorabiloo;

import Emtity.ListEmployee;
import Emtity.ListHoSo;
import Emtity.Users;
import Model.ConnectedDatabase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author admin
 */
public class TruongPhong {
    ConnectedDatabase cnn = new ConnectedDatabase();
    ArrayList<ListHoSo> dsHS = new ArrayList<>();
    
    public ArrayList<ListEmployee> LoadTable(String manv,int idPhong){
        ArrayList<ListEmployee> dsNV = new ArrayList<>();
        try {
            System.out.println(manv);
            cnn.ConnectedDB();
            try {        
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users Where manv != ? And phong_id = ?");
            stmt.setString(1, manv);
            stmt.setInt(2, idPhong);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                String mnv = result.getString("manv");
                String ten = result.getString("fullname");
                int document_up = result.getInt("total_document_up");
                int document_ori = result.getInt("total_document_Obligatory");
                String status = null;
                if(document_up == document_ori){
                    status = "Đã nộp đủ";
                }else{
                    status = "Chưa nộp đủ";
                }
                dsNV.add(new ListEmployee(mnv, ten, status));
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return dsNV;
    }
    
    public ArrayList<Users> LoadIdPhong(String manv){
        ArrayList<Users> phong = new ArrayList<>();
        try {
            cnn.ConnectedDB();      
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users where manv = ?");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                int idPhong = result.getInt("phong_id");
                System.out.println(idPhong);
                phong.add(new Users(idPhong));
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return phong;
    }
    public boolean pheDuyet(int idDoc){  
        int row = 0;
        try{
            cnn.ConnectedDB();
            try{
                PreparedStatement stmt = cnn.connection.prepareStatement("update user_has_documents set status = ? where id = ?");
                stmt.setInt(1, 5);
                stmt.setInt(2, idDoc);
                row = stmt.executeUpdate();
                if(row>0){
                  return true; 
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null,e);
        }
        }catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
       
        }
        return false;
    }
    public boolean KhongpheDuyet(int idDoc){
        try{
            cnn.ConnectedDB();
            PreparedStatement stmt = cnn.connection.prepareStatement("UPDATE user_has_documents set status = ? where id = ?");
            stmt.setInt(1, 4);
            stmt.setInt(2, idDoc);
            stmt.executeUpdate();
            return true;   
        }catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
       
        }
        return false;
    }
}