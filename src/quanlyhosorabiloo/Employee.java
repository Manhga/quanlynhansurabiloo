/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosorabiloo;

import Emtity.ListHoSo;
import Emtity.PhanQuyen;
import Emtity.UserDetail;
import Emtity.UserHasVehicle;
import Emtity.Users;
import Model.ConnectedDatabase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author manhg
 */
public class Employee {
    ConnectedDatabase cnn = new ConnectedDatabase();
    ArrayList<ListHoSo> dsHS = new ArrayList<>();
    
    public ArrayList<ListHoSo> getDsHS() {
        return dsHS;
    }
    
    public void setDsPQ(ArrayList<ListHoSo> dsHS) {
        this.dsHS = dsHS;
    }
    
    public ArrayList<ListHoSo> LoadTable(String manv){
        ArrayList<ListHoSo> dsHS = new ArrayList<>();
        try {
            cnn.ConnectedDB();
            try {        
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM user_has_documents, document Where manv = ? and user_has_documents.id_document = document.id");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                int id = result.getInt(1);
//                System.out.println(id);
                int id_document = result.getInt(3);
                String ten = result.getString(6);
                int status_id = result.getInt(4);
//                System.out.println(result.getInt(4));
                PreparedStatement st = cnn.connection.prepareStatement("SELECT * FROM Status Where id = ?");
                st.setInt(1, status_id);
                ResultSet re = st.executeQuery();
                while(re.next()){
                    String status = re.getString(2);
                    dsHS.add(new ListHoSo(id, id_document, ten, status, status_id));
                }
                st.close();
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return dsHS;
    }
    public ArrayList<ListHoSo> LoadTable(Emtity.Employee emp){
        ArrayList<ListHoSo> dsHS = new ArrayList<>();
        try {
            cnn.ConnectedDB();
            try {        
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM user_has_documents, document Where manv = ? and user_has_documents.id_document = document.id");
            stmt.setString(1,emp.getManv());
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                int id = result.getInt(1);
//                System.out.println(id);
                int id_document = result.getInt(3);
                String ten = result.getString(6);
                int status_id = result.getInt(4);
//                System.out.println(result.getInt(4));
                PreparedStatement st = cnn.connection.prepareStatement("SELECT * FROM Status Where id = ?");
                st.setInt(1, status_id);
                ResultSet re = st.executeQuery();
                while(re.next()){
                    String status = re.getString(2);
                    dsHS.add(new ListHoSo(id, id_document, ten, status, status_id));
                }
                st.close();
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(LoadPhanQuyen.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return dsHS;
    }
    
    
    public Vector getResident(){
        Vector v = new Vector();
        try{
            cnn.ConnectedDB();
            PreparedStatement stt = cnn.connection.prepareStatement("SELECT * FROM type_of_residencs");
            ResultSet result = stt.executeQuery();
            while(result.next()){
                v.add(result.getString(2));
            }
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null,"Lỗi rồi");
        }
       return v;
    }
    
    public Vector getRelationShip(){
        Vector v = new Vector();
        try{
            cnn.ConnectedDB();
            PreparedStatement stt = cnn.connection.prepareStatement("SELECT * FROM Relationship");
            ResultSet result = stt.executeQuery();
            while(result.next()){
                v.add(result.getString(2));
            }
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null,"Lỗi rồi");
        }
       return v;
    }
    
    public Vector getLevel(){
        Vector v = new Vector();
        try{
            cnn.ConnectedDB();
            PreparedStatement stt = cnn.connection.prepareStatement("SELECT * FROM Level");
            ResultSet result = stt.executeQuery();
            while(result.next()){
                v.add(result.getString(2));
            }
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null,"Lỗi rồi");
        }
       return v;
    }
    
    public ArrayList<UserDetail> LoadDetail(String manv){
        ArrayList<UserDetail> userDetail = new ArrayList<>();
        try{
            cnn.ConnectedDB();
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM detail_users Where manv = ?");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                int levelId = result.getInt("Level_id");
                String education = result.getString("education");
                String address = result.getString("address");
                int id_type_residence = result.getInt("id_type_of_residence");
                String nameOfHost = result.getString("name_of_host");
                String samePerSon = result.getString("the_same_person");
                int relationshipId = result.getInt("relationship_id");
                String userComment = result.getString("user_comment");
                String managerComment = result.getString("manager_comment");
                int phone = result.getInt("phone");
                String familyName = result.getString("family_user_name");
                String familyAddress = result.getString("family_address");
                userDetail.add(new UserDetail(
                        levelId, education, address, id_type_residence,
                        nameOfHost, samePerSon, relationshipId, userComment, managerComment,
                        phone, familyName, familyAddress
                ));
            }
            stmt.close();
            
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Lỗi");
        }
        return userDetail;
    }
    
    public ArrayList<Users> LoadFullname(String manv){
        ArrayList<Users> fullName = new ArrayList<>();
        try {
            cnn.ConnectedDB();      
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users where manv = ?");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                String mnv = result.getString("manv");
                String name = result.getString("fullname");
                int document = result.getInt("total_document_up");
                System.out.println(name);
                fullName.add(new Users(mnv, name, document));
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return fullName;
    }
    
    public ArrayList<Users> LoadUser(String manv){
        ArrayList<Users> dsUS = new ArrayList<>();
        try {
            cnn.ConnectedDB();      
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users where manv = ?");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                String mnv = result.getString("manv");
                int doc_put = result.getInt("total_document_up");
                dsUS.add(new Users(mnv, doc_put));
            }
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return dsUS;
    }
    
    public boolean insertDB(
            int statusId, String manv, String employee_cm,String phone,int doc_put,
            String manager_cmt, String theSamePerson, int typeResidence,
            String address, String familyName, int relationshipId, String familyAddress,
            int levelId, String education, String nameOfHost, int idDoc
    ){
        try{
            cnn.ConnectedDB();
            
            try{
                if(statusId == 1){
                PreparedStatement stmt = cnn.connection.prepareStatement("Insert into detail_users values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.setString(1, manv);
                stmt.setInt(2, levelId);
                stmt.setString(3, education);
                stmt.setString(4, address);
                stmt.setInt(5, typeResidence);
                stmt.setString(6, nameOfHost);
                stmt.setString(7, theSamePerson);
                stmt.setString(8, familyName);
                stmt.setString(9, familyAddress);
                stmt.setInt(10, relationshipId);
                stmt.setString(11, employee_cm);
                stmt.setString(12, manager_cmt);
                stmt.setString(13, phone);
                stmt.executeUpdate();
                doc_put = doc_put+1;
                //Update docput trong bảng Users
                PreparedStatement st = cnn.connection.prepareStatement("Update Users set total_document_up =? where manv= ?");
                st.setInt(1, doc_put);
                st.setString(2, manv);
                st.executeUpdate();
                //Update status trong bảng user_has_documents
                PreparedStatement us = cnn.connection.prepareStatement("Update user_has_documents set status =? where id= ?");
                us.setInt(1, 2);
                us.setInt(2, idDoc);
                us.executeUpdate();
                return true;
                }else{
                    PreparedStatement stmt = cnn.connection.prepareStatement(
                            "Update detail_users set Level_id =?, education=?, address=?,"
                                    + "id_type_of_residence=?, name_of_host=?, the_same_person=?,"
                                    +"family_user_name=?, family_address=?, relationship_id=?,"
                                    + "user_comment=?, manager_comment=?, phone=? where manv= ?"
                    );
                    stmt.setInt(1, levelId);
                    stmt.setString(2, education);
                    stmt.setString(3, address);
                    stmt.setInt(4, typeResidence);
                    stmt.setString(5, nameOfHost);
                    stmt.setString(6, theSamePerson);
                    stmt.setString(7, familyName);
                    stmt.setString(8, familyAddress);
                    stmt.setInt(9, relationshipId);
                    stmt.setString(10, employee_cm);
                    stmt.setString(11, manager_cmt);
                    stmt.setString(12, phone);
                    stmt.setString(13, manv);
                    stmt.executeUpdate();
                    PreparedStatement us = cnn.connection.prepareStatement("Update user_has_documents set status =? where id= ?");
                    us.setInt(1, 2);
                    us.setInt(2, statusId);
                    us.executeUpdate();
                    return true;
                }
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null,e);
            }finally{
                CloseConnect();
            }
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null,ex);
        }
        return false;
    }
    
    //Hồ sơ đi lại
    public Vector getVehicle(){
        Vector v = new Vector();
        try{
            cnn.ConnectedDB();
            PreparedStatement stt = cnn.connection.prepareStatement("SELECT * FROM Vehicles");
            ResultSet result = stt.executeQuery();
            while(result.next()){
                v.add(result.getString(2));
            }
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null,"Lỗi rồi");
        }
       return v;
    }
    
    public ArrayList<UserHasVehicle> LoadUserVehicle(String manv){
        ArrayList<UserHasVehicle> userVehicle = new ArrayList<>();
        try{
            cnn.ConnectedDB();
            PreparedStatement stmt = cnn.connection.prepareStatement("SELECT * FROM Users_has_Vehicles Where manv = ?");
            stmt.setString(1, manv);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                int vehicleId = result.getInt("Vehicle_id");
                int distance = result.getInt("distance");
                int time = result.getInt("time_run");
                Date date = result.getDate("start_date");
                userVehicle.add(new UserHasVehicle(
                vehicleId, distance, time, date
                ));
            }
            stmt.close();
            
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, ex);
        }
        return userVehicle;
    }
    
    public boolean insertVehicles(
            int vehicleId, int distance, int time, String date,
            int statusId ,int idDoc, int doc_put, String manv
    ){
        try{
            cnn.ConnectedDB();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.sql.Date startDate;
            try {
                startDate = new java.sql.Date(formatter.parse(date).getTime());
                System.out.println(startDate);
                try{
                if(statusId == 1){
                PreparedStatement stmt = cnn.connection.prepareStatement("Insert into Users_has_Vehicles values(?,?,?,?,?)");
                stmt.setString(1, manv);
                stmt.setInt(2, vehicleId);
                stmt.setInt(3, distance);
                stmt.setInt(4, time);
                stmt.setDate(5, startDate);
                stmt.executeUpdate();
                doc_put = doc_put+1;
                //Update docput trong bảng Users
                PreparedStatement st = cnn.connection.prepareStatement("Update Users set total_document_up =? where manv= ?");
                st.setInt(1, doc_put);
                st.setString(2, manv);
                st.executeUpdate();
                //Update status trong bảng user_has_documents
                PreparedStatement us = cnn.connection.prepareStatement("Update user_has_documents set status =? where id= ?");
                us.setInt(1, 2);
                us.setInt(2, idDoc);
                us.executeUpdate();
                return true;
                }else{
                    PreparedStatement stmt = cnn.connection.prepareStatement(
                            "Update Users_has_Vehicles set Vehicle_id = ?, distance = ?, time_run = ?, start_date = ?"
                    );
                    stmt.setInt(1, vehicleId);
                    stmt.setInt(2, distance);
                    stmt.setInt(3, time);
                    stmt.setDate(4, startDate);
                    stmt.executeUpdate();
                    PreparedStatement us = cnn.connection.prepareStatement("Update user_has_documents set status =? where id= ?");
                    us.setInt(1, 2);
                    us.setInt(2, statusId);
                    us.executeUpdate();
                    return true;
                }
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null,"Lỗi sql");
            }finally{
                CloseConnect();
            }

            } catch (ParseException ex) {
                Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }catch(SQLException e){
                
        }
        return false;
    }
    private void CloseConnect() {
         try {
                cnn.connection.close();
                
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
    }
    
}
