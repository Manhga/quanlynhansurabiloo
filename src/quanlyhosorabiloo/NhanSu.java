/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosorabiloo;

import Model.ConnectedDatabase;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author hoang
 */
public class NhanSu {
    ConnectedDatabase cnn = new ConnectedDatabase();
    public boolean pheDuyet(int idDoc){  
        int row = 0;
        try{
            cnn.ConnectedDB();
            try{
                PreparedStatement stmt = cnn.connection.prepareStatement("update user_has_documents set status = ? where id = ?");
                stmt.setInt(1, 3);
                stmt.setInt(2, idDoc);
                row = stmt.executeUpdate();
                if(row>0){
                  return true; 
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null,e);
        }
        }catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
       
        }
        return false;
    }
    public boolean KhongpheDuyet(int idDoc){
        try{
            cnn.ConnectedDB();
            PreparedStatement stmt = cnn.connection.prepareStatement("UPDATE user_has_documents set status = ? where id = ?");
            stmt.setInt(1, 4);
            stmt.setInt(2, idDoc);
            stmt.executeUpdate();
            return true;   
        }catch (SQLException ex) {
            Logger.getLogger(AccountAction.class.getName()).log(Level.SEVERE, null, ex);
       
        }
        return false;
    }
}
