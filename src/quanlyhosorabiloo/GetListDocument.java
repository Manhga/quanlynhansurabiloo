/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosorabiloo;

import Model.ConnectedDatabase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author hoang
 */
public  class GetListDocument {
    ConnectedDatabase cnn = new ConnectedDatabase();
    public class TableListTaiLieu {
    
    private String manv,tennv,ngaysinh,phong, tiendo;

        public String getManv() {
            return manv;
        }

        public void setManv(String manv) {
            this.manv = manv;
        }

        public String getTennv() {
            return tennv;
        }

        public void setTennv(String tennv) {
            this.tennv = tennv;
        }

        public String getNgaysinh() {
            return ngaysinh;
        }

        public void setNgaysinh(String ngaysinh) {
            this.ngaysinh = ngaysinh;
        }

        public String getPhong() {
            return phong;
        }

        public void setPhong(String phong) {
            this.phong = phong;
        }

        public String getTiendo() {
            return tiendo;
        }

        public void setTiendo(String tiendo) {
            this.tiendo = tiendo;
        }
    
    }
    public ArrayList<TableListTaiLieu> getList() {
        ArrayList<TableListTaiLieu> listTaiLieu =  new ArrayList<>();
        
        if(AccountAction.role == 3)
        {
            try {
                cnn.ConnectedDB();
                PreparedStatement stm = cnn.connection.prepareStatement("SELECT fullname, birthday, Users.manv, name, total_document_Obligatory, total_document_up FROM Users inner join user_has_roles on Users.manv = user_has_roles.manv inner join Phong on Users.phong_id = Phong.id  where user_has_roles.id_role = ?");
                stm.setInt(1, 2);
                ResultSet result = stm.executeQuery();
            while(result.next())
            {
                TableListTaiLieu tb = new TableListTaiLieu();
                tb.setManv(result.getString("manv"));
                tb.setNgaysinh(result.getString("birthday"));
                tb.setPhong(result.getString("name"));
                tb.setTennv(result.getString("fullname"));
                int tiendo = result.getInt("total_document_Obligatory") - result.getInt("total_document_up");
                if (tiendo == 0) {
                    tb.setTiendo("Hoàn thành");
                } else {
                    tb.setTiendo("Chưa hoàn thành");
                }
                listTaiLieu.add(tb);
            }
            } catch (SQLException e) {
            }
        }
        return listTaiLieu;
    }
}

